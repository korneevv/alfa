package org.stepik.korneev.alfaLab;

import org.junit.Test;

import static org.junit.Assert.*;

public class SeparatorTest {

	@Test
	public void splitBaseTest() {
		String reference = "сапог сарай арбуз болт бокс биржа";
		String excepted = "[б=[биржа, бокс, болт], с=[сапог, сарай]]";
		assertEquals(excepted, Separator.split(reference));
	}

	@Test
	public void splitAdvancedTest() {
		String reference = "1234 azxzxz 1222 фыяу qwe фаык 1111 wer файйй asdw abvc фйййй azxz 2111 avcd";
		String excepted = "[1=[1111, 1222, 1234], a=[azxzxz, abvc, asdw, avcd, azxz], ф=[файйй, фйййй, фаык, фыяу]]";
		assertEquals(excepted, Separator.split(reference));
	}
}
