package org.stepik.korneev.alfaLab;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class Separator {

    public static String split(String str) {
        Map<Character, List<String>> wordsByFirstChar = Stream.of(str.split(" "))
                .collect(Collectors.groupingBy(it -> it.charAt(0)));

        return wordsByFirstChar.values().stream()
                .map(WordGroup::new)
                .filter(group -> group.getSize() > 1)
                .sorted()
                .map(WordGroup::toString)
                .collect(Collectors.joining(", ", "[", "]"));
    }
}

class WordGroup implements Comparable<WordGroup> {

    private char id;
    private Set<String> words = new TreeSet<>(new WordGroupComparator());
    public static class WordGroupComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            int result = Integer.compare(o2.length(), o1.length());
            return result == 0 ? o1.compareTo(o2) : result;
        }
    }

    public WordGroup(Collection<String> words) {
        if (words == null || words.isEmpty()) {
            throw new IllegalArgumentException("Collection must be not null");
        }

        if (words.stream().map(it -> it.charAt(0)).collect(toSet()).size() != 1) {
            throw new IllegalArgumentException("Collection must be contain words starting with the same char");
        }

        this.words.addAll(words);
        this.id = this.words.iterator().next().charAt(0);
    }

    public int getSize() {
        return words.size();
    }

    @Override
    public String toString() {
        return id + "=" + words.stream().collect(Collectors.joining(", ", "[", "]"));
    }

    @Override
    public int compareTo(WordGroup o) {
        return Character.compare(id, o.id);
    }
}
